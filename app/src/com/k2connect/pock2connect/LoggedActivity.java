package com.k2connect.pock2connect;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;

import static com.k2connect.pock2connect.MediaTypeUtil.IMAGE;

public class LoggedActivity extends Activity {

    private final int WRITE_REQUEST_CODE = 1001;
    private final String PREFS_NAME = "Preferences";
    private final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1001;
    private final int REQUEST_CODE_GALLERY = 1011;
    private Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged);
    }


    public void onFotoClicked(View v) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = new MediaTypeUtil().getMediaFileUri(IMAGE);
        //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        //takePictureIntent.putExtra("return-data", true);
        startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE: {
                if (resultCode == RESULT_OK) {
                   // handleCameraPhoto(data);
                    Log.d("debug", "Foto");
                }
                break;
            } // ACTION_TAKE_PHOTO


        }

    }

//    public void getGallery(View v) {
//
//        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//
//        startActivityForResult(i,REQUEST_CODE_GALLERY);
//    }
//
//    public void sendRequest(){
//        //RestClient unauthenticatedRestClient = clientManager.peekUnauthenticatedRestClient(); RestRequest request = new RestRequest(RestMethod.GET, "https://api.spotify.com/v1/search?q=James%20Brown&type=artist", null);
//       // RestResponse response = unauthenticatedRestClient.sendSync(request);
//    }

}
