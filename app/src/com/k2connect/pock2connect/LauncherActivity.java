package com.k2connect.pock2connect;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LauncherActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
    }

    /*
     * TODO: Add logic here to determine if we are already
     * logged in, and skip this screen by calling
     * 'finish()', if that is the case.
    */
    public void onLoginClicked(View v) {
        final Intent mainIntent =
                new Intent(this, LoggedActivity.class);
        mainIntent.addCategory(Intent.CATEGORY_DEFAULT);
        startActivity(mainIntent);
        finish();

    }

}




