package com.k2connect.pock2connect;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.ContentValues.TAG;

/**
 * Created by iomarsantos on 03/12/16.
 */

public class MediaTypeUtil {

    public static final int IMAGE = 0, VIDEO = 1;

    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

    private static final String DIRECTORY_NAME = "nicbrain_supervisor";

    public Uri getMediaFileUri(int type) {
        return Uri.fromFile(type == 0 ? getMediaFilePicture() : getMediaFileVideo());
    }

    private File getMediaFileVideo() {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), DIRECTORY_NAME);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Erro! Falhou ao criar o " + DIRECTORY_NAME + " directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");

        return mediaFile;
    }


    private File getMediaFilePicture() {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DIRECTORY_NAME);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Erro! Falhou ao criar o " + DIRECTORY_NAME + " directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".png");

        return mediaFile;
    }
}
